SET AUTOCOMMIT=1;

DROP DATABASE shop;

CREATE DATABASE IF NOT EXISTS shop;

USE shop;



CREATE TABLE IF NOT EXISTS shop.EMPLEAT (
                                            EMPLEAT_ID    SMALLINT (4) UNSIGNED,
                                            COGNOM    VARCHAR (10) NOT NULL,
                                            OFICI     VARCHAR (10),
                                            CAP_ID       SMALLINT (4) UNSIGNED,
                                            DATA_ALTA DATE,
                                            SALARI    INT UNSIGNED,
                                            COMISSIO  INT UNSIGNED,
                                            DEPT_NO   TINYINT (2) UNSIGNED NOT NULL,
                                            PRIMARY KEY (EMPLEAT_ID));

INSERT INTO shop.EMPLEAT VALUES (7369,'S�NCHEZ','EMPLEAT',7902, '1980-12-17', 104000,NULL,20);
INSERT INTO shop.EMPLEAT VALUES (7499,'ARROYO','VENEDOR',7698, '1980-02-20', 208000,39000,30);
INSERT INTO shop.EMPLEAT VALUES (7521,'SALA','VENEDOR',7698, '1981-02-22', 162500,65000,30);
INSERT INTO shop.EMPLEAT VALUES (7566,'JIM�NEZ','DIRECTOR',7839, '1981-04-02', 386750,NULL,20);
INSERT INTO shop.EMPLEAT VALUES (7654,'MART�N','VENEDOR',7698, '1981-09-29', 162500,182000,30);
INSERT INTO shop.EMPLEAT VALUES (7698,'NEGRO','DIRECTOR',7839, '1981-05-01', 370500,NULL,30);
INSERT INTO shop.EMPLEAT VALUES (7782,'CEREZO','DIRECTOR',7839, '1981-06-09', 318500,NULL,10);
INSERT INTO shop.EMPLEAT VALUES (7788,'GIL','ANALISTA',7566, '1981-11-09', 390000,NULL,20);
INSERT INTO shop.EMPLEAT VALUES (7839,'REY','PRESIDENT',NULL, '1981-11-17', 650000,NULL,10);
INSERT INTO shop.EMPLEAT VALUES (7844,'TOVAR','VENEDOR',7698, '1981-09-08', 195000,0,30);
INSERT INTO shop.EMPLEAT VALUES (7876,'ALONSO','EMPLEAT',7788, '1981-09-23', 143000,NULL,20);
INSERT INTO shop.EMPLEAT VALUES (7900,'JIMENO','EMPLEAT',7698, '1981-12-03', 123500,NULL,30);
INSERT INTO shop.EMPLEAT VALUES (7902,'FERN�NDEZ','ANALISTA',7566, '1981-12-03', 390000,NULL,20);
INSERT INTO shop.EMPLEAT VALUES (7934,'MU�OZ','EMPLEAT',7782, '1982-01-23', 169000,NULL,10);


CREATE TABLE IF NOT EXISTS shop.PROV (
                                         ID_PROV          INT(6) UNSIGNED PRIMARY KEY,
                                         NOM                 VARCHAR (45) NOT NULL,
                                         ADRECA              VARCHAR (40) NOT NULL,
                                         CIUTAT              VARCHAR (30) NOT NULL,
                                         ESTAT               VARCHAR (2),
                                         CODI_POSTAL         VARCHAR (9) NOT NULL,
                                         AREA                SMALLINT(3),
                                         TELEFON             VARCHAR (9),
                                         ID_PRODUCTE         INT (6) UNSIGNED,
                                         QUANTITAT           INT (8) UNSIGNED,
                                         LIMIT_CREDIT        DECIMAL(9,2) UNSIGNED,
                                         OBSERVACIONS        TEXT);


INSERT INTO shop.PROV (ID_PROV , NOM, ADRECA, CIUTAT, ESTAT, CODI_POSTAL, AREA,
                       TELEFON, ID_PRODUCTE ,QUANTITAT, LIMIT_CREDIT, OBSERVACIONS)
VALUES (100, 'JOCKSPORTS', '345 VIEWRIDGE', 'BELMONT', 'CA', '96711', 415,
        '598-6609', 100860,350,5000,
        'Very friendly people to work with -- sales rep likes to be called Mike.');
INSERT INTO shop.PROV (ID_PROV , NOM, ADRECA, CIUTAT, ESTAT, CODI_POSTAL, AREA,
                       TELEFON, ID_PRODUCTE ,QUANTITAT, LIMIT_CREDIT, OBSERVACIONS)
VALUES (101, 'TKB SPORT SHOP', '490 BOLI RD.', 'REDWOOD CIUTAT', 'CA', '94061', 415,
        '368-1223',100861, 752, 10000,
        'Rep called 5/8 about change in order - contact shipping.');
INSERT INTO shop.PROV (ID_PROV , NOM, ADRECA, CIUTAT, ESTAT, CODI_POSTAL, AREA,
                       TELEFON, ID_PRODUCTE ,QUANTITAT, LIMIT_CREDIT, OBSERVACIONS)
VALUES (102, 'VOLLYRITE', '9722 HAMILTON', 'BURLINGAME', 'CA', '95133', 415,
        '644-3341',100871, 800, 7000,
        'Company doing heavy promotion beginning 10/89. Prepare for large orders during winter.');
INSERT INTO shop.PROV (ID_PROV , NOM, ADRECA, CIUTAT, ESTAT, CODI_POSTAL, AREA,
                       TELEFON, ID_PRODUCTE ,QUANTITAT, LIMIT_CREDIT, OBSERVACIONS)
VALUES (103, 'JUST TENNIS', 'HILLVIEW MALL', 'BURLINGAME', 'CA', '97544', 415,
        '677-9312',100890, 400, 3000,
        'Contact rep about new line of tennis rackets.');
INSERT INTO shop.PROV (ID_PROV , NOM, ADRECA, CIUTAT, ESTAT, CODI_POSTAL, AREA,
                       TELEFON, ID_PRODUCTE ,QUANTITAT, LIMIT_CREDIT, OBSERVACIONS)
VALUES (104, 'EVERY MOUNTAIN', '574 SURRY RD.', 'CUPERTINO', 'CA', '93301', 408,
        '996-2323',101860, 250, 10000,
        'CLIENT with high market share (23%) due to aggressive advertising.');
INSERT INTO shop.PROV (ID_PROV , NOM, ADRECA, CIUTAT, ESTAT, CODI_POSTAL, AREA,
                       TELEFON, ID_PRODUCTE ,QUANTITAT, LIMIT_CREDIT, OBSERVACIONS)
VALUES (105, 'K + T SPORTS', '3476 EL PASEO', 'SANTA CLARA', 'CA', '91003', 408,
        '376-9966',101863, 625, 5000,
        'Tends to order large amounts of merchandise at once. Accounting is considering raising their credit limit. Usually pays on time.');
INSERT INTO shop.PROV (ID_PROV , NOM, ADRECA, CIUTAT, ESTAT, CODI_POSTAL, AREA,
                       TELEFON, ID_PRODUCTE ,QUANTITAT, LIMIT_CREDIT, OBSERVACIONS)
VALUES (106, 'SHAPE UP', '908 SEQUOIA', 'PALO ALTO', 'CA', '94301', 415,
        '364-9777',102130, 280, 6000,
        'Support intensive. Orders small amounts (< 800) of merchandise at a time.');
INSERT INTO shop.PROV (ID_PROV , NOM, ADRECA, CIUTAT, ESTAT, CODI_POSTAL, AREA,
                       TELEFON, ID_PRODUCTE ,QUANTITAT, LIMIT_CREDIT, OBSERVACIONS)
VALUES (107, 'WOMEN SPORTS', 'VALCO VILLAGE', 'SUNNYVALE', 'CA', '93301', 408,
        '967-4398',200376, 2000, 10000,
        'First sporting goods store geared exclusively towards women. Unusual promotion al style and very willing to take chances towards new PRODUCTEs!');
INSERT INTO shop.PROV (ID_PROV , NOM, ADRECA, CIUTAT, ESTAT, CODI_POSTAL, AREA,
                       TELEFON, ID_PRODUCTE ,QUANTITAT, LIMIT_CREDIT, OBSERVACIONS)
VALUES (108, 'NORTH WOODS HEALTH AND FITNESS SUPPLY CENTER', '98 LONE PINE WAY', 'HIBBING', 'MN', '55649', 612,
        '566-9123',100870, 800, 8000, '');
INSERT INTO shop.PROV (ID_PROV, NOM, ADRECA, CIUTAT, ESTAT, CODI_POSTAL, AREA,
                       TELEFON, ID_PRODUCTE ,QUANTITAT, LIMIT_CREDIT, OBSERVACIONS)
VALUES (109, 'SPRINGFIELD NUCLEAR POWER PLANT', '13 PERCEBE STR.', 'SPRINGFIELD', 'NT', '0000', 636,
        '999-6666', 200380, 700,10000, '');



CREATE TABLE IF NOT EXISTS shop.PRODUCTE (
                                             ID_PRODUCTE    INT (6) UNSIGNED PRIMARY KEY,
                                             DESCRIPCIO     VARCHAR (30) NOT NULL  UNIQUE,
                                             STOCKACTUAL    INT (8) UNSIGNED,
                                             STOCKMINIM     INT (8) UNSIGNED,
                                             PREU           DECIMAL(8,2) UNSIGNED);



INSERT INTO shop.PRODUCTE (ID_PRODUCTE, DESCRIPCIO, STOCKACTUAL, STOCKMINIM,PREU)
VALUES (100860, 'ACE TENNIS RACKET I', 10, 5, 308);
INSERT INTO shop.PRODUCTE (ID_PRODUCTE, DESCRIPCIO, STOCKACTUAL, STOCKMINIM,PREU)
VALUES (100861, 'ACE TENNIS RACKET II', 10, 5, 15.275);
INSERT INTO shop.PRODUCTE (ID_PRODUCTE, DESCRIPCIO, STOCKACTUAL, STOCKMINIM,PREU)
VALUES (100870, 'ACE TENNIS BALLS-3 PACK',10,5,40.6);
INSERT INTO shop.PRODUCTE (ID_PRODUCTE, DESCRIPCIO, STOCKACTUAL, STOCKMINIM,PREU)
VALUES (100871, 'ACE TENNIS BALLS-6 PACK',10,5,33.2);
INSERT INTO shop.PRODUCTE (ID_PRODUCTE, DESCRIPCIO, STOCKACTUAL, STOCKMINIM,PREU)
VALUES (100890, 'ACE TENNIS NET',10,5,30);
INSERT INTO shop.PRODUCTE (ID_PRODUCTE, DESCRIPCIO, STOCKACTUAL, STOCKMINIM,PREU)
VALUES (101860, 'SP TENNIS RACKET',10,5,548);
INSERT INTO shop.PRODUCTE (ID_PRODUCTE, DESCRIPCIO, STOCKACTUAL, STOCKMINIM,PREU)
VALUES (101863, 'SP JUNIOR RACKET',10,5,124);
INSERT INTO shop.PRODUCTE (ID_PRODUCTE, DESCRIPCIO, STOCKACTUAL, STOCKMINIM,PREU)
VALUES (102130, 'RH: "GUIDE TO TENNIS"',10,5,48);
INSERT INTO shop.PRODUCTE (ID_PRODUCTE, DESCRIPCIO, STOCKACTUAL, STOCKMINIM,PREU)
VALUES (200376, 'SB ENERGY BAR-6 PACK',10,5,16);
INSERT INTO shop.PRODUCTE (ID_PRODUCTE, DESCRIPCIO, STOCKACTUAL, STOCKMINIM,PREU)
VALUES (200380, 'SB VITA SNACK-6 PACK',10,5,24);


CREATE TABLE IF NOT EXISTS shop.COMANDA  (
                                             ID_COMANDA          SMALLINT(4) UNSIGNED PRIMARY KEY,
                                             ID_PRODUCTE 	     INT (6) UNSIGNED,
                                             DATA_COMANDA        DATE,
                                             QUANTITAT           INT (8) UNSIGNED,
                                             ID_PROV          INT (6) UNSIGNED NOT NULL,
                                             DATA_TRAMESA        DATE,
                                             TOTAL               DECIMAL(8,2) UNSIGNED);



INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (610,100860, '1987-01-07', 10, 101, '1987-01-08', 101.4);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (611,102130, '1987-01-11', 15, 102,'1987-01-11', 45);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (612,100870, '1987-01-15', 20, 104, '1987-01-20', 5860);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (601,102130, '1986-05-01', 80, 106, '1986-05-30', 200.4);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (602,101863, '1986-06-05', 60, 102, '1986-06-20', 560);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (604,100871, '1986-06-15', 65, 106, '1986-06-30', 698);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (605,200380, '1986-07-14', 80, 106,  '1986-07-30', 8324);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (606,101863, '1986-07-14', 25, 100,  '1986-07-30', 300.4);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (609,100870, '1986-08-01', 45, 100,  '1986-08-15', 97.5);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (607,100871, '1986-07-18', 90, 104,  '1986-07-18', 500.6);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (608,200376, '1986-07-25', 70, 104,  '1986-07-25', 350.2);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (603,200380, '1986-06-05', 65, 102, '1986-06-05', 224);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (620,100890, '1987-03-12', 100, 100, '1987-03-12', 4450);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (613,100870, '1987-02-01', 89, 108, '1987-02-01', 6400);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (614,102130, '1987-02-01', 110, 102, '1987-02-05', 23940);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (616,100871,'1987-02-03', 46, 103, '1987-02-10', 764);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (619,100890, '1987-02-22', 87, 104, '1987-02-04', 1260);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (617,101863, '1987-02-05', 60, 104, '1987-03-03', 46370);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (615,102130, '1987-02-01', 80, 107, '1987-02-06', 710);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (618,100890, '1987-02-15', 20, 102, '1987-03-06', 3510.5);
INSERT INTO shop.COMANDA (ID_COMANDA,ID_PRODUCTE,DATA_COMANDA, QUANTITAT,ID_PROV,DATA_TRAMESA,TOTAL)
VALUES (621,100890, '1987-03-15', 98, 100, '1987-01-01', 730);






