package m06.uf4;

import m06.uf4.DAO.comanda.Comanda;
import m06.uf4.DAO.comanda.ComandaImpSQL;
import m06.uf4.DAO.empleat.Empleat;
import m06.uf4.DAO.empleat.EmpleatImpSQL;
import m06.uf4.DAO.producte.Producte;
import m06.uf4.DAO.producte.ProducteImpSQL;
import m06.uf4.DAO.proveidor.Proveidor;
import m06.uf4.factories.DAOFactory;
import m06.uf4.factories.ShopDAOFactoryMongo;
import m06.uf4.factories.ShopDAOFactorySQL;
import m06.uf4.factories.ShopDAOFactorySerializable;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        Main program = new Main();
        program.start();
    }

    private void start()
    {
        ShopDAOFactorySerializable factorySerializable = (ShopDAOFactorySerializable) DAOFactory.getDaoFactory(DAOFactory.DatabaseType.SERIALIZABLE);
        ShopDAOFactoryMongo factoryMongo = (ShopDAOFactoryMongo) DAOFactory.getDaoFactory(DAOFactory.DatabaseType.MONGO);
        ShopDAOFactorySQL factorySQL = (ShopDAOFactorySQL) DAOFactory.getDaoFactory(DAOFactory.DatabaseType.SQL);

        assert factorySerializable != null;
        assert factoryMongo != null;
        assert factorySQL != null;

        factoryMongo.getEmpleatImplementation().deleteWhole();
        factoryMongo.getProveidorImplementation().deleteWhole();
        factoryMongo.getComandaImplementation().deleteWhole();
        factoryMongo.getProducteImplementation().deleteWhole();


        //EXERCICI 1

        System.out.println("EXERCICI 1");

        List<Empleat> empleats = factorySQL.getEmpleatImplementation().getWhole();
        factorySerializable.getEmpleatImplementation().insert(empleats);

        System.out.println("DONE");

        //EXERCICI 2

        System.out.println("EXERCICI 2");

        List<Empleat> empleats2 = factorySerializable.getEmpleatImplementation().getWhole();
        factoryMongo.getEmpleatImplementation().insert(empleats2);

        System.out.println("DONE");

        //EXERCICI 3

        System.out.println("EXERCICI 3");

        List<Comanda> comandas = factorySQL.getComandaImplementation().getWhole();
        factorySerializable.getComandaImplementation().insert(comandas);

        System.out.println("DONE");

        //EXERCICI 4
        System.out.println("EXERCICI 4");

        List<Comanda> comandas2 = factorySerializable.getComandaImplementation().getWhole();
        factoryMongo.getComandaImplementation().insert(comandas2);

        System.out.println("DONE");

        //EXERCICI 5
        System.out.println("EXERCICI 5");

        List<Producte> productes = factorySQL.getProducteImplementation().getWhole();
        factorySerializable.getProducteImplementation().insert(productes);

        System.out.println("DONE");

        //EXERCICI 6
        System.out.println("EXERCICI 6");

        List<Producte> productes2 = factorySerializable.getProducteImplementation().getWhole();
        factoryMongo.getProducteImplementation().insert(productes2);

        System.out.println("DONE");

        //EXERCICI 7
        System.out.println("EXERCICI 7");

        List<Proveidor> proveidors = factorySQL.getProveidorImplementation().getWhole();
        factorySerializable.getProveidorImplementation().insert(proveidors);

        System.out.println("DONE");

        //EXERCICI 8
        List<Proveidor> proveidors2 = factorySerializable.getProveidorImplementation().getWhole();
        factoryMongo.getProveidorImplementation().insert(proveidors2);

        System.out.println("DONE");

        //EXERCICI 9
        System.out.println("EXERCICI 9");

        System.out.println("Empleat: " + factoryMongo.getEmpleatImplementation().get(7499));

        System.out.println("DONE");

        //EXERCICI 10
        System.out.println("EXERCICI 10");

        System.out.println("Deleted successfully: " + factoryMongo.getEmpleatImplementation().delete(7369));

        System.out.println("DONE");

        //EXERCICI 11
        System.out.println("EXERCICI 11");

        EmpleatImpSQL empleatImpSql = factorySQL.getEmpleatImplementation();

        Empleat empleat = empleatImpSql.get(7654);
        empleat.setOfici("ANALISTA");

        empleatImpSql.modify(empleat);

        System.out.println("Modified employee");
        System.out.println("DONE");

        //EXERCICI 12
        System.out.println("EXERCICI 12");

        ComandaImpSQL comandaImpMongo = factorySQL.getComandaImplementation();
        ProducteImpSQL producteImpSQL = factorySQL.getProducteImplementation();

        List<Comanda> comandas3 = comandaImpMongo.getWhole();
        comandas3.forEach(comanda ->
        {
            double priceProduct = producteImpSQL.get(comanda.getIdProducte()).getPreu();
            comanda.setTotal(priceProduct * comanda.getQuantitat());
        });

        comandaImpMongo.insert(comandas3);

        System.out.println("DONE");

        //EXERCICI 13, 14
        System.out.println("EXERCICI 13/14");

        Producte producte = factorySQL.getProducteImplementation().get(101863L);
        long producteId = producte.getIdProducte();
        Proveidor proveidor = factorySQL.getProveidorImplementation().getWhole().stream().filter(prov -> prov.getIdProducte() == producteId).findFirst().get();

        final long productsQuantity = 25L;
        java.sql.Date today = java.sql.Date.valueOf(LocalDate.now());

        producte.addPropertyChangeListener(new Comanda(622, producteId, today, proveidor.getIdProducte(), productsQuantity, addDays(today, 4), producte.getPreu() * productsQuantity));

        producte.setStockActual(2L);

        System.out.println("DONE");
    }

    public static java.sql.Date addDays(java.sql.Date date, int days)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, days);
        return new java.sql.Date(c.getTimeInMillis());
    }
}
