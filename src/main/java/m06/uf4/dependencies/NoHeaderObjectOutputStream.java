package m06.uf4.dependencies;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class NoHeaderObjectOutputStream extends ObjectOutputStream
{
    public NoHeaderObjectOutputStream(OutputStream out) throws IOException { super(out); }

    protected NoHeaderObjectOutputStream() throws IOException, SecurityException { }

    @Override
    protected void writeStreamHeader() { }
}
