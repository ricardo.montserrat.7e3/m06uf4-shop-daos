package m06.uf4.dependencies;

import com.google.gson.Gson;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


public class MyMongoDatabase
{
    public static final String SERVER_MONGO_IP = "mongodb+srv://user1:9878JCv@cluster0.yjrbj.azure.mongodb.net/";
    public static final String VIRTUAL_MACHINE_IP = "192.168.56.101:27017";

    private MongoClient client;

    public MyMongoDatabase(final String ip) { init(ip, false); }

    public MyMongoDatabase(final String ip, final boolean isServer) { init(ip, isServer); }

    private void init(final String ip, final boolean isServer)
    {
        CodecRegistry pojoCodecRegistry = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        MongoClientOptions.Builder optionsBuilder = MongoClientOptions.builder().codecRegistry(pojoCodecRegistry);

        if (isServer) client = new MongoClient(new MongoClientURI(ip, optionsBuilder));
        client = new MongoClient(new ServerAddress(ip), optionsBuilder.build());
    }

    //region COLLECTION_METHODS
    private String readFile(String path)
    {
        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(path)))
        {
            stringFile = br.lines().collect(Collectors.joining());
        }
        catch (Exception e) { System.out.println(e.toString()); }
        return stringFile;
    }

    public <X> X[] getJsonArray(String json, Type type) { return new Gson().fromJson(json, type); }

    //region COLLECTION_METHODS
    public long loadArrayJson(final String database, final String collection, final String filePath)
    {
        Document[] documents = getJsonArray(readFile(filePath), Document[].class);
        getCollection(database, collection).insertMany(Arrays.asList(documents));
        return documents.length;
    }

    public <X> long loadArrayJson(final String database, final String collection, final String filePath, Class<X> xClass)
    {
        X[] documents = getJsonArray(readFile(filePath), xClass.arrayType());
        getCollection(database, collection, xClass).insertMany(Arrays.asList(documents));
        return documents.length;
    }

    public MongoCollection<Document> getCollection(final String database, final String collection) { return client.getDatabase(database).getCollection(collection); }

    public <X> MongoCollection<X> getCollection(String database, String collection, Class<X> type) { return client.getDatabase(database).getCollection(collection, type); }

    public void showCollections(final String database) { client.getDatabase(database).listCollectionNames().iterator().forEachRemaining(collection -> System.out.println(collection + ", ")); }

    public void showCollectionIndexes(final String database, final String collection) { getCollection(database, collection).listIndexes().iterator().forEachRemaining(System.out::println);}

    public long dropCollection(final String database, final String collection)
    {
        MongoCollection<Document> toDestroy = client.getDatabase(database).getCollection(collection);
        long collectionSize = toDestroy.countDocuments();
        toDestroy.drop();
        return collectionSize;
    }

    public void createIndex(final String database, final String collection, final String index, final boolean unique)
    {
        try { getCollection(database, collection).createIndex(Indexes.text(index), new IndexOptions().unique(unique)); }
        catch (DuplicateKeyException e)
        {
            System.out.printf("The index \"%s\" was duplicated in a field of the database \"%s\" inside the collection \"%s\"; so it couldn't be created!",
                    index, database, collection);
        }
    }

    //endregion ---------------------------------------------------------------------------------

    //region SEARCH_METHODS

    //region BASE_METHODS

    public void showArrayField(final String field, final String database, final String collection)
    {
        getCollection(database, collection).distinct(field, String.class).iterator().forEachRemaining(System.out::println);
    }

    public void showField(final String field, final String database, final String collection)
    {
        getCollection(database, collection).find().projection(Projections.fields(Projections.include(field), Projections.excludeId())).iterator().forEachRemaining(System.out::println);
    }

    public void showDocuments(final String database, final String collection, Bson filter, int limit, Bson sortType, Bson projection)
    {
        getCollection(database, collection).find(filter).limit(limit).sort(sortType).projection(projection).iterator().forEachRemaining(System.out::println);
    }
    //endregion ---------------------------------------------------------------------------------

    public void showDocumentsWhere(final String field, final String[] valueField, final String database, final String collection, final int limit, final Bson optionalFilter, final Bson optionalSortBy, final String... includeDocuments)
    {
        showDocuments(database, collection,
                Filters.and(Filters.all(field, valueField), optionalFilter),
                limit,
                optionalSortBy,
                Projections.fields(Projections.include(includeDocuments), Projections.excludeId()));
    }

    public void showDocumentsWhere(final Bson filter, final String database, final String collection, final int limit, final Bson optionalSortBy, final String... includeDocuments)
    {
        showDocuments(database, collection, filter,
                limit,
                optionalSortBy,
                Projections.fields(Projections.include(includeDocuments), Projections.excludeId()));
    }

    public void showDocumentsWhere(final String field, final String[] valueField, final String database, final String collection, final int limit, final Bson optionalFilter, final String... includeDocuments)
    {
        showDocumentsWhere(field, valueField, database, collection, limit, optionalFilter, Sorts.ascending(), includeDocuments);
    }

    public void showDocumentsWhere(final String field, final String[] valueField, final String database, final String collection, final int limit, final String... includeDocuments)
    {
        showDocumentsWhere(field, valueField, database, collection, limit, Filters.and(), Sorts.ascending(), includeDocuments);
    }

    public void showDocumentsWhere(final String field, final String valueField, final String database, final String collection, final int limit, final Bson optionalFilter, final Bson optionalSortBy, final String... includeDocuments)
    {
        showDocuments(database, collection,
                Filters.and(Filters.eq(field, valueField), optionalFilter),
                limit,
                optionalSortBy,
                Projections.fields(Projections.include(includeDocuments), Projections.excludeId()));
    }

    public void showDocumentsWhere(final String field, final String valueField, final String database, final String collection, final int limit, final Bson optionalFilter, final String... includeDocuments)
    {
        showDocumentsWhere(field, valueField, database, collection, limit, optionalFilter, Sorts.ascending(), includeDocuments);
    }

    public void showDocumentsWhere(final String field, final String valueField, final String database, final String collection, final int limit, final String... includeDocuments)
    {
        showDocumentsWhere(field, valueField, database, collection, limit, Filters.and(), Sorts.ascending(), includeDocuments);
    }

    public void showDocumentsWhereHas(final String field, final String[] valueField, final String database, final String collection, final int limit, final Bson optionalFilter, final Bson optionalSortBy, final String... includeDocuments)
    {
        showDocuments(database, collection,
                Filters.and(Filters.in(field, valueField), optionalFilter),
                limit,
                optionalSortBy,
                Projections.fields(Projections.include(includeDocuments), Projections.excludeId()));
    }

    public void showDocumentsWhereHas(final String field, final String[] valueField, final String database, final String collection, final int limit, final String... includeDocuments)
    {
        showDocuments(database, collection,
                Filters.and(Filters.in(field, valueField), Filters.and()),
                limit,
                Sorts.ascending(),
                Projections.fields(Projections.include(includeDocuments), Projections.excludeId()));
    }

    public void showDocumentsWhereBetween(final String field, final double minValueField, final double maxValueField, final String database, final String collection, final Bson optionalFilter, final String... includeDocuments)
    {
        showDocuments(database, collection,
                Filters.and(Updates.combine(Filters.gte(field, minValueField), Filters.lte(field, maxValueField)), optionalFilter),
                0,
                Sorts.ascending(),
                Projections.fields(Projections.include(includeDocuments), Projections.excludeId()));
    }

    public void showDocumentsByGroupSize(final String group, final String database, final String collection)
    {
        getCollection(database, collection).aggregate(Arrays.asList(Aggregates.group('$' + group, Accumulators.sum("totalCount", 1)), Aggregates.sort(Sorts.descending("totalCount")))
        ).iterator().forEachRemaining(System.out::println);
    }

    public void showAverageDocumentsByGroupSize(final String group, final String database, final String collection)
    {
        getCollection(database, collection).aggregate(Arrays.asList(Aggregates.group('$' + group, Accumulators.avg("totalCount", 1)), Aggregates.sort(Sorts.descending("totalCount")))
        ).iterator().forEachRemaining(System.out::println);
    }

    public void showDocumentQuantityByArrayFieldValues(final String field, final String database, final String collection)
    {
        getCollection(database, collection).aggregate(
                Arrays.asList(Aggregates.unwind("$" + field),
                        Aggregates.group("$" + field, Accumulators.sum("categories", 1)),
                        Aggregates.sort(Sorts.descending("categories")))).iterator().forEachRemaining(System.out::println);
    }

    //endregion ---------------------------------------------------------------------------------

    //region UPDATE_METHODS

    //region BASE_METHODS
    public boolean updateDocument(final String database, final String collection, final Bson filter, final Bson update)
    {
        return getCollection(database, collection).updateOne(filter, update).wasAcknowledged();
    }
    public boolean updateDocumentById(final String database, final String collection, String id, final Bson update)
    {
        return getCollection(database, collection).updateOne(Filters.eq("_id", id), update).wasAcknowledged();
    }

    public long updateDocuments(final String database, final String collection, final Bson filter, final Bson update)
    {
        return getCollection(database, collection).updateMany(filter, update).getModifiedCount();
    }

    public void insertDocument(final String database, final String collection, Document document) { getCollection(database, collection).insertOne(document); }

    public void insertDocuments(final String database, final String collection, Document... documents) { getCollection(database, collection).insertMany(Arrays.asList(documents)); }

    @SuppressWarnings("unchecked")
    public <X> void insertDocument(final String database, final String collection, X document) { getCollection(database, collection, (Class<X>) document.getClass()).insertOne(document); }

    @SuppressWarnings("unchecked")
    public final <X> void insertDocuments(final String database, final String collection, X... documents) { getCollection(database, collection, (Class<X>) documents[0].getClass()).insertMany(Arrays.asList(documents)); }

    @SuppressWarnings("unchecked")
    public final <X> void insertDocuments(final String database, final String collection, List<X> documents) { getCollection(database, collection, (Class<X>) documents.get(0).getClass()).insertMany(documents); }

    public boolean deleteDocument(final String database, final String collection, final Bson filter)
    {
        return getCollection(database, collection).deleteOne(filter).wasAcknowledged();
    }

    public boolean deleteDocumentById(final String database, final String collection, String id)
    {
        return getCollection(database, collection).deleteOne(Filters.eq("_id", id)).wasAcknowledged();
    }

    public long deleteDocuments(final String database, final String collection, final Bson filter)
    {
        return getCollection(database, collection).deleteMany(filter).getDeletedCount();
    }
    //endregion ---------------------------------------------------------------------------------

    public long deleteDocumentsWhereBetween(final String database, final String collection, final String field, int minValue, int maxValue)
    {
        return deleteDocuments(database, collection, Updates.combine(Filters.gte(field, minValue), Filters.lte(field, maxValue)));
    }

    public long updateDocumentsWhereBetween(final String database, final String collection, final String fieldToCompare, int minValue, int maxValue, final String fieldToChange, int newValue)
    {
        return updateDocuments(database, collection, Updates.combine(Filters.gte(fieldToCompare, minValue), Filters.lte(fieldToCompare, maxValue)), Updates.set(fieldToChange, newValue));
    }

    public long setField(final String field, final String value, final String database, final String collection, final Bson optionalFilter) { return updateDocuments(database, collection, optionalFilter, Updates.set(field, value)); }

    public long addToField(final String field, final String value, final String database, final String collection, final Bson optionalFilter) { return updateDocuments(database, collection, optionalFilter, Updates.addToSet(field, value)); }

    public long setField(final String field, final String value, final String database, final String collection) { return setField(field, value, database, collection, Filters.exists(field)); }

    public long addToField(final String field, final String value, final String database, final String collection) { return addToField(field, value, database, collection, Filters.and()); }

    public <X> X getDocument(final String database, final String collection, final Bson filter, Class<X> type)
    {
        return getCollection(database, collection, type).find(filter).first();
    }

    public List<Document> getDocuments(final String database, final String collection)
    {
        List<Document> result = new ArrayList<>();
        getCollection(database, collection).find().iterator().forEachRemaining(result::add);
        return result;
    }

    public <X> List<X> getDocuments(final String database, final String collection, Class<X> type)
    {
        List<X> result = new ArrayList<>();
        getCollection(database, collection, type).find().iterator().forEachRemaining(result::add);
        return result;
    }

    public Document getDocument(final String database, final String collection, final Bson filter)
    {
        return getCollection(database, collection).find(filter).first();
    }

    public <X> X getDocumentById(final String database, final String collection, final String idField, final String id, Class<X> type)
    {
        return getDocument(database, collection, Filters.eq(idField, id), type);
    }

    public Document getDocumentById(final String database, final String collection, final String id)
    {
        return getDocument(database, collection, Filters.eq("_id", id));
    }

    //endregion ---------------------------------------------------------------------------------

    //region AGGREGATES_TEST

    public void getCountOfEnglishSpeakingCountries(final String database, final String collection)
    {
        Document englishSpeakingCountries = getCollection(database, collection).aggregate(Arrays.asList(Aggregates.match(Filters.eq("languages.name", "English")), Aggregates.count())).first();
        assert englishSpeakingCountries != null;
        System.out.println("English speaking countries: " + englishSpeakingCountries.get("count"));
    }

    public void checkIfRegionWithMoreCountriesIsAfrica(final String database, final String collection)
    {
        Document maxCountriedRegion = getCollection(database, collection).aggregate(Arrays.asList(Aggregates.group("$region", Accumulators.sum("totalCount", 1)), Aggregates.sort(Sorts.descending("totalCount")))).first();

        assert maxCountriedRegion != null;
        System.out.println("Is Africa the region with more countries? " + maxCountriedRegion.containsValue("Africa"));
    }

    public void createLargest7CountriesCollection(final String database, final String collection)
    {
        getCollection(database, collection).aggregate(Arrays.asList(
                Aggregates.sort(Sorts.descending("area")),
                Aggregates.limit(7),
                Aggregates.out("largest_seven"))).toCollection();

        MongoCollection<Document> largestSeven = getCollection(database, "largest_seven");

        System.out.println("Are the countries limited to 7? " + (largestSeven.countDocuments() == 7));

        Document usa = largestSeven.find(Filters.eq("alpha3Code", "USA")).first();

        System.out.println("Is USA in the largest seven? " + (usa != null));
    }

    public void seeCountryWithMoreNeighbors(final String database, final String collection)
    {
        Bson borderingCountriesCollection = Aggregates.project(Projections.fields(Projections.excludeId(),
                Projections.include("name"), Projections.computed("borderingCountries",
                        Projections.computed("$size", "$borders"))));

        MongoCollection<Document> col = getCollection(database, collection);
        int maxValue = Objects.requireNonNull(
                col.aggregate(Arrays.asList(borderingCountriesCollection,
                        Aggregates.group(null, Accumulators.max("max", "$borderingCountries"))))
                        .first()).getInteger("max");

        System.out.println("Is the max value 15? " + (maxValue == 15));

        Document maxNeighboredCountry = col.aggregate(Arrays.asList(borderingCountriesCollection,
                Aggregates.match(Filters.eq("borderingCountries", maxValue)))).first();

        assert maxNeighboredCountry != null;
        System.out.println("Is China the max neighbored country? " + maxNeighboredCountry.containsValue("China"));
    }

    //endregion ---------------------------------------------------------------------------------
}
