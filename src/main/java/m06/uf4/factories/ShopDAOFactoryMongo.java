package m06.uf4.factories;

import m06.uf4.DAO.bases.ShopDAOFactory;
import m06.uf4.DAO.bases.Values;
import m06.uf4.DAO.comanda.ComandaImpMongo;
import m06.uf4.DAO.empleat.EmpleatImpMongo;
import m06.uf4.DAO.producte.ProducteImpMongo;
import m06.uf4.DAO.proveidor.ProveidorImpMongo;

import java.beans.PropertyChangeSupport;

public class ShopDAOFactoryMongo implements ShopDAOFactory<ComandaImpMongo, EmpleatImpMongo, ProducteImpMongo, ProveidorImpMongo>
{
    public PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    @Override
    public ComandaImpMongo getComandaImplementation() { return new ComandaImpMongo(Values.IP_MONGO_MACHINE); }

    @Override
    public EmpleatImpMongo getEmpleatImplementation() { return new EmpleatImpMongo(Values.IP_MONGO_MACHINE); }

    @Override
    public ProducteImpMongo getProducteImplementation() { return new ProducteImpMongo(Values.IP_MONGO_MACHINE); }

    @Override
    public ProveidorImpMongo getProveidorImplementation() { return new ProveidorImpMongo(Values.IP_MONGO_MACHINE); }
}
