package m06.uf4.factories;

import m06.uf4.DAO.bases.ShopDAOFactory;
import m06.uf4.DAO.comanda.ComandaImpSerializable;
import m06.uf4.DAO.empleat.EmpleatImpSerializable;
import m06.uf4.DAO.producte.ProducteImpSerializable;
import m06.uf4.DAO.proveidor.ProveidorImpSerializable;

import java.beans.PropertyChangeSupport;

public class ShopDAOFactorySerializable implements ShopDAOFactory<
        ComandaImpSerializable, EmpleatImpSerializable, ProducteImpSerializable, ProveidorImpSerializable>
{

    public PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    @Override
    public ComandaImpSerializable getComandaImplementation() { return new ComandaImpSerializable(); }

    @Override
    public EmpleatImpSerializable getEmpleatImplementation() { return new EmpleatImpSerializable(); }

    @Override
    public ProducteImpSerializable getProducteImplementation() { return new ProducteImpSerializable(); }

    @Override
    public ProveidorImpSerializable getProveidorImplementation() { return new ProveidorImpSerializable(); }
}
