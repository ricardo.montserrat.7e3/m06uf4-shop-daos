package m06.uf4.factories;

import m06.uf4.DAO.bases.ShopDAOFactory;
import m06.uf4.DAO.comanda.Comanda;
import m06.uf4.DAO.comanda.ComandaImpSQL;
import m06.uf4.DAO.empleat.Empleat;
import m06.uf4.DAO.empleat.EmpleatImpSQL;
import m06.uf4.DAO.producte.Producte;
import m06.uf4.DAO.producte.ProducteImpSQL;
import m06.uf4.DAO.proveidor.Proveidor;
import m06.uf4.DAO.proveidor.ProveidorImpSQL;

import java.beans.PropertyChangeListener;

public class DAOFactory
{
    public enum DatabaseType
    { SQL, MONGO, SERIALIZABLE }

    public static ShopDAOFactory getDaoFactory(final DatabaseType databaseType)
    {
        PropertyChangeListener propertyChangeListener = evt ->
        {
            updateDatabases(evt.getNewValue(), evt.getPropertyName());
        };

        switch (databaseType)
        {
            case SQL -> {
                ShopDAOFactorySQL daoFactorySQL = new ShopDAOFactorySQL();
                daoFactorySQL.propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
                return daoFactorySQL;
            }
            case MONGO -> {
                ShopDAOFactoryMongo daoFactoryMongo = new ShopDAOFactoryMongo();
                daoFactoryMongo.propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
                return daoFactoryMongo;
            }
            case SERIALIZABLE -> {
                ShopDAOFactorySerializable daoFactorySerializable = new ShopDAOFactorySerializable();
                daoFactorySerializable.propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
                return daoFactorySerializable;
            }
            default -> {
                System.out.println("Type doesn't exist!");
                return null;
            }
        }
    }

    public static <T> void updateDatabases(T obj, String databaseType)
    {
        if (obj instanceof Comanda comanda)
        {
            ComandaImpSQL daoFactorySQL = new ShopDAOFactorySQL().getComandaImplementation();
            if (databaseType.equals(DatabaseType.SQL.name()))
            {
                new ShopDAOFactoryMongo().getComandaImplementation().modify(comanda);
            }
            else if (databaseType.equals(DatabaseType.MONGO.name()))
            {
                daoFactorySQL.modify(comanda);
            }
            new ShopDAOFactorySerializable().getComandaImplementation().insert(daoFactorySQL.getWhole());
        }
        else if (obj instanceof Empleat empleat)
        {
            EmpleatImpSQL daoFactorySQL = new ShopDAOFactorySQL().getEmpleatImplementation();
            if (databaseType.equals(DatabaseType.SQL.name()))
            {
                new ShopDAOFactoryMongo().getEmpleatImplementation().modify(empleat);
            }
            else if (databaseType.equals(DatabaseType.MONGO.name()))
            {
                daoFactorySQL.modify(empleat);
            }
            new ShopDAOFactorySerializable().getEmpleatImplementation().insert(daoFactorySQL.getWhole());
        }
        else if (obj instanceof Producte producte)
        {
            ProducteImpSQL daoFactorySQL = new ShopDAOFactorySQL().getProducteImplementation();
            if (databaseType.equals(DatabaseType.SQL.name()))
            {
                new ShopDAOFactoryMongo().getProducteImplementation().modify(producte);
            }
            else if (databaseType.equals(DatabaseType.MONGO.name()))
            {
                daoFactorySQL.modify(producte);
            }
            new ShopDAOFactorySerializable().getProducteImplementation().insert(daoFactorySQL.getWhole());
        }
        else if (obj instanceof Proveidor proveidor)
        {
            ProveidorImpSQL daoFactorySQL = new ShopDAOFactorySQL().getProveidorImplementation();
            if (databaseType.equals(DatabaseType.SQL.name()))
            {
                new ShopDAOFactoryMongo().getProveidorImplementation().modify(proveidor);
            }
            else if (databaseType.equals(DatabaseType.MONGO.name()))
            {
                daoFactorySQL.modify(proveidor);
            }
            new ShopDAOFactorySerializable().getProveidorImplementation().insert(daoFactorySQL.getWhole());
        }
    }
}
