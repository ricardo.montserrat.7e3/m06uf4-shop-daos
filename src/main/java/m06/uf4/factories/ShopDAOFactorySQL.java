package m06.uf4.factories;

import m06.uf4.DAO.bases.ShopDAOFactory;
import m06.uf4.DAO.bases.Values;
import m06.uf4.DAO.comanda.ComandaImpSQL;
import m06.uf4.DAO.empleat.EmpleatImpSQL;
import m06.uf4.DAO.producte.ProducteImpSQL;
import m06.uf4.DAO.proveidor.ProveidorImpSQL;

import java.beans.PropertyChangeSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ShopDAOFactorySQL implements ShopDAOFactory<
        ComandaImpSQL, EmpleatImpSQL, ProducteImpSQL, ProveidorImpSQL>
{
    private static Connection connection;

    public PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public static Connection createConnection()
    {
        return connection != null ? connection : createConnection(Values.URL, Values.DB, Values.USER, Values.PASSWORD);
    }

    public static Connection createConnection(String url, String bd, String user, String password)
    {
        try
        {
            return connection = DriverManager.getConnection(url + bd, user, password);
        }
        catch (SQLException e)
        {
            exceptionMessage(e);
            return connection = null;
        }
    }

    @Override
    public ComandaImpSQL getComandaImplementation() { return new ComandaImpSQL(); }

    @Override
    public EmpleatImpSQL getEmpleatImplementation() { return new EmpleatImpSQL(); }

    @Override
    public ProducteImpSQL getProducteImplementation() { return new ProducteImpSQL(); }

    @Override
    public ProveidorImpSQL getProveidorImplementation() { return new ProveidorImpSQL(); }

    public static void exceptionMessage(SQLException e)
    {
        System.out.println("An exception ocurred:");
        System.out.printf("Mensaje : %s %n", e.getMessage());
        System.out.printf("SQL estado: %s %n", e.getSQLState());
        System.out.printf("Cód error : %s %n", e.getErrorCode());
    }
}
