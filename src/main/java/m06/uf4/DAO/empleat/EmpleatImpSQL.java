package m06.uf4.DAO.empleat;

import m06.uf4.DAO.bases.SQLImplementation;
import m06.uf4.DAO.bases.Values;

import java.util.ArrayList;

// EMPLEAT_ID, COGNOM, OFICI, CAP, DATA_ALTA, SALARI, COMISSIO, DEPT_NO

public class EmpleatImpSQL extends SQLImplementation<Empleat, Integer>
{
    public EmpleatImpSQL()
    {
        super(Values.COLLECTION_EMPLEAT, "empleat_id", 8);
    }

    @Override
    public Object[] getObjectParameters(Empleat value)
    {
        return new Object[]
                {
                        value.getEmpleatID(),
                        value.getCognom(),
                        value.getOfici(),
                        value.getCapId(),
                        value.getDataAlta(),
                        value.getSalari(),
                        value.getComissio(),
                        value.getDepNo()
                };
    }

    @Override
    public Empleat castToObject(ArrayList<Object> params)
    {
        Empleat empleat = new Empleat();

        Object value = params.get(0);
        empleat.setEmpleatID(value != null? (Integer) value : null);

        value = params.get(1);
        empleat.setCognom(value != null? (String) value : null);

        value = params.get(2);
        empleat.setOfici(value != null? (String) value : null);

        value = params.get(3);
        empleat.setCapId(value != null ? (Integer) value : null);

        value = params.get(4);
        empleat.setDataAlta(value != null ? (java.sql.Date) value : null);

        value = params.get(5);
        empleat.setSalari(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(6);
        empleat.setComissio(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(7);
        empleat.setDepNo(value != null ? (Integer) value : null);
        return empleat;
    }

    @Override
    public boolean modify(Empleat value)
    {
        try
        {
            sqlPetition(String.format(
                    """
                            UPDATE %s 
                            SET empleat_id = ?, cognom = ?, ofici = ?, cap_id = ?, data_alta = ?, salari = ?,  comissio = ?, dept_no = ? 
                            WHERE empleat_id = ?
                            """,
                    tableName), columns,true, getObjectParameters(value), value.getEmpleatID());
            return true;
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
            e.printStackTrace();
            return false;
        }
    }
}
