package m06.uf4.DAO.empleat;

import m06.uf4.DAO.bases.SerializableImplementation;
import m06.uf4.DAO.bases.Values;

import java.util.List;

public class EmpleatImpSerializable extends SerializableImplementation<Empleat, Integer>
{
    public EmpleatImpSerializable() { super(Values.FILE_EMPLEAT); }

    @Override
    public Empleat get(Integer valueId)
    {
        List<Empleat> empleats = getWhole();
        for (Empleat empleat : empleats)
        {
            if(empleat.getEmpleatID().equals(valueId)) return empleat;
        }
        return null;
    }
}
