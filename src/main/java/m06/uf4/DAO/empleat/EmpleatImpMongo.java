package m06.uf4.DAO.empleat;

import com.mongodb.client.model.Updates;
import m06.uf4.DAO.bases.MongoImplementation;
import m06.uf4.DAO.bases.Values;

public class EmpleatImpMongo extends MongoImplementation<Empleat, Integer>
{
    public EmpleatImpMongo(String ip) {
        super(ip, Values.DATABASE, Values.COLLECTION_EMPLEAT, "empleatID", Empleat.class);
    }

    public EmpleatImpMongo(String ip, boolean isServer) { super(ip, isServer, "empleatID", Values.DATABASE, Values.COLLECTION_EMPLEAT, Empleat.class); }

    @Override
    public boolean modify(Empleat value)
    {
        return mongo.updateDocumentById(database, collection, value.getEmpleatID() + "", Updates.combine(
                Updates.set("cognom", value.getCognom()),
                Updates.set("ofici", value.getOfici()),
                Updates.set("dataAlta", value.getDataAlta()),
                Updates.set("salari", value.getSalari()),
                Updates.set("comissio", value.getComissio()),
                Updates.set("depNo", value.getDepNo())
        ));
    }
}
