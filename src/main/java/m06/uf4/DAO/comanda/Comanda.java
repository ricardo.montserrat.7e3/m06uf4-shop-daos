package m06.uf4.DAO.comanda;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import m06.uf4.DAO.proveidor.Proveidor;
import m06.uf4.DAO.proveidor.ProveidorImpSQL;
import m06.uf4.factories.DAOFactory;
import m06.uf4.factories.ShopDAOFactorySQL;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;

/*
   ID_COMANDA          SMALLINT(4) UNSIGNED PRIMARY KEY,
   ID_PRODUCTE 	     INT (6) UNSIGNED,
   DATA_COMANDA        DATE,
   QUANTITAT           INT (8) UNSIGNED,
   ID_PROV          INT (6) UNSIGNED NOT NULL,
   DATA_TRAMESA        DATE,
   TOTAL               DECIMAL(8,2) UNSIGNED
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Comanda implements Serializable, PropertyChangeListener
{
    private Integer idComanda;
    private Long idProducte;
    private java.sql.Date dataComanda;
    private Long quantitat, idProv;
    private java.sql.Date dataTramesa;
    private Double total;

    public void propertyChange(PropertyChangeEvent evt)
    {
        if (evt.getPropertyName().equals("stockActual"))
        {
            //EXERCICI 13
            System.out.printf("Old Stock: %s %nActual Stock: %s %nDemanding more products for %s%n", evt.getOldValue(), evt.getNewValue(), idProducte);
            ShopDAOFactorySQL shopDAOFactorySQL = (ShopDAOFactorySQL) DAOFactory.getDaoFactory(DAOFactory.DatabaseType.SQL);

            assert shopDAOFactorySQL != null;
            shopDAOFactorySQL.getComandaImplementation().insert(this);

            //EXERCICI 14
            ProveidorImpSQL proveidorImpSQL = shopDAOFactorySQL.getProveidorImplementation();

            Proveidor proveidor = proveidorImpSQL.get(idProv);
            proveidor.setQuantitat(proveidor.getQuantitat() + quantitat);

            proveidorImpSQL.modify(proveidor);
        }
    }
}
