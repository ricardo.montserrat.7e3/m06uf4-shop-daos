package m06.uf4.DAO.comanda;

import m06.uf4.DAO.bases.SerializableImplementation;
import m06.uf4.DAO.bases.Values;

import java.util.List;

public class ComandaImpSerializable extends SerializableImplementation<Comanda, Integer>
{
    public ComandaImpSerializable() { super(Values.FILE_COMANDA); }

    @Override
    public Comanda get(Integer valueId)
    {
        List<Comanda> comandas = getWhole();
        for (Comanda comanda : comandas)
        {
            if(comanda.getIdComanda().equals(valueId)) return comanda;
        }
        return null;
    }
}
