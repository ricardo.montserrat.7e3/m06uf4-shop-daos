package m06.uf4.DAO.comanda;

import m06.uf4.DAO.bases.SQLImplementation;
import m06.uf4.DAO.bases.Values;

import java.math.BigDecimal;
import java.util.ArrayList;

// EMPLEAT_ID, COGNOM, OFICI, CAP, DATA_ALTA, SALARI, COMISSIO, DEPT_NO

public class ComandaImpSQL extends SQLImplementation<Comanda, Integer>
{
    public ComandaImpSQL()
    {
        super(Values.COLLECTION_COMANDA, "id_comanda", 7);
    }

    @Override
    public Object[] getObjectParameters(Comanda value)
    {
        return new Object[]
                {
                        value.getIdComanda(),
                        value.getIdProducte(),
                        value.getDataComanda(),
                        value.getQuantitat(),
                        value.getIdProv(),
                        value.getDataTramesa(),
                        value.getTotal()
                };
    }

    @Override
    public Comanda castToObject(ArrayList<Object> params)
    {
        Comanda comanda = new Comanda();

        Object value = params.get(0);
        comanda.setIdComanda(value != null? (Integer) value : null);

        value = params.get(1);
        comanda.setIdProducte(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(2);
        comanda.setDataComanda(value != null ? (java.sql.Date) value : null);

        value = params.get(3);
        comanda.setQuantitat(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(4);
        comanda.setIdProv(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(5);
        comanda.setDataTramesa(value != null ? (java.sql.Date) value : null);

        value = params.get(6);
        comanda.setTotal(value != null ? value instanceof Double ? (Double) value : ((BigDecimal) value).doubleValue() : null);

        return comanda;
    }

    @Override
    public boolean modify(Comanda value)
    {
        try
        {
            sqlPetition(String.format(
                    """
                            UPDATE %s 
                            SET id_comanda = ?, id_producte = ?, data_comanda = ?, quantitat = ?, id_prov = ?, data_tramesa = ?, total = ? 
                            WHERE id_comanda = ?
                            """,
                    tableName), columns,true, getObjectParameters(value), value.getIdComanda());
            return true;
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
            e.printStackTrace();
            return false;
        }
    }
}
