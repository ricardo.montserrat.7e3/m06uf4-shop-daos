package m06.uf4.DAO.comanda;

import com.mongodb.client.model.Updates;
import m06.uf4.DAO.bases.MongoImplementation;
import m06.uf4.DAO.bases.Values;

public class ComandaImpMongo extends MongoImplementation<Comanda, Integer>
{
    public ComandaImpMongo(String ip)
    {
        super(ip, Values.DATABASE, Values.COLLECTION_COMANDA, "idComanda", Comanda.class);
    }

    public ComandaImpMongo(String ip, boolean isServer) { super(ip, isServer, "idComanda", Values.DATABASE, Values.COLLECTION_COMANDA, Comanda.class); }

    @Override
    public boolean modify(Comanda value)
    {
        return mongo.updateDocumentById(database, collection, value.getIdComanda() + "", Updates.combine(
                Updates.set("idComanda", value.getDataComanda()),
                Updates.set("quantitat", value.getQuantitat()),
                Updates.set("dataTramesa", value.getDataTramesa()),
                Updates.set("total", value.getTotal())
        ));
    }
}
