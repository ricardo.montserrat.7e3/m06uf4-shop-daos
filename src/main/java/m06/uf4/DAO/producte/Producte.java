package m06.uf4.DAO.producte;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.codecs.pojo.annotations.BsonIgnore;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/*
     ID_PRODUCTE    INT (6) UNSIGNED PRIMARY KEY,
     DESCRIPCIO     VARCHAR (30) NOT NULL  UNIQUE,
     STOCKACTUAL    INT (8) UNSIGNED,
     STOCKMINIM     INT (8) UNSIGNED,
     PREU           DECIMAL(8,2) UNSIGNED
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Producte implements Serializable
{
    private Long idProducte;
    private String descripcio;
    private Long stockActual, stockMinim;
    private Double preu;

    @BsonIgnore
    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void setStockActual(Long newValue)
    {
        if (stockMinim != null && newValue != null && newValue < stockMinim) propertyChangeSupport.firePropertyChange("stockActual", stockActual, newValue);
        stockActual = newValue;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
}
