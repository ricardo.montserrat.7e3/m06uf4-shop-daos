package m06.uf4.DAO.producte;

import m06.uf4.DAO.bases.SQLImplementation;
import m06.uf4.DAO.bases.Values;

import java.math.BigDecimal;
import java.util.ArrayList;

// EMPLEAT_ID, COGNOM, OFICI, CAP, DATA_ALTA, SALARI, COMISSIO, DEPT_NO

public class ProducteImpSQL extends SQLImplementation<Producte, Long>
{
    public ProducteImpSQL()
    {
        super(Values.COLLECTION_PRODUCTE, "id_producte", 5);
    }

    @Override
    public Object[] getObjectParameters(Producte value)
    {
        return new Object[]
                {
                        value.getIdProducte(),
                        value.getDescripcio(),
                        value.getStockActual(),
                        value.getStockMinim(),
                        value.getPreu()
                };
    }

    @Override
    public Producte castToObject(ArrayList<Object> params)
    {
        Producte producte = new Producte();

        Object value = params.get(0);
        producte.setIdProducte(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(1);
        producte.setDescripcio(value != null? (String) value : null);

        value = params.get(2);
        producte.setStockActual(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(3);
        producte.setStockMinim(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(4);
        producte.setPreu(value != null ? value instanceof Double ? (Double) value : ((BigDecimal) value).doubleValue() : null);

        return producte;
    }

    @Override
    public boolean modify(Producte value)
    {
        try
        {
            sqlPetition(String.format(
                    """
                            UPDATE %s 
                            SET id_producte = ?, descripcio = ?, stockactual = ?, stockminim = ?, preu = ?
                            WHERE id_producte = ?
                            """,
                    tableName), columns, true, getObjectParameters(value), value.getIdProducte());
            return true;
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
            e.printStackTrace();
            return false;
        }
    }
}
