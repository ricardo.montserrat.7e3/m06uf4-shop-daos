package m06.uf4.DAO.producte;

import com.mongodb.client.model.Updates;
import m06.uf4.DAO.bases.MongoImplementation;
import m06.uf4.DAO.bases.Values;

public class ProducteImpMongo extends MongoImplementation<Producte, Long>
{
    public ProducteImpMongo(String ip)
    {
        super(ip, Values.DATABASE, Values.COLLECTION_PRODUCTE, "idProducte", Producte.class);
    }

    public ProducteImpMongo(String ip, boolean isServer) { super(ip, isServer, Values.DATABASE, Values.COLLECTION_PRODUCTE, "idProducte", Producte.class); }

    @Override
    public boolean modify(Producte value)
    {
        return mongo.updateDocumentById(database, collection, value.getIdProducte() + "", Updates.combine(
                Updates.set("descripcio", value.getDescripcio()),
                Updates.set("stockMinim", value.getStockMinim()),
                Updates.set("stockActual", value.getStockActual()),
                Updates.set("preu", value.getPreu())
        ));
    }
}
