package m06.uf4.DAO.producte;

import m06.uf4.DAO.bases.SerializableImplementation;
import m06.uf4.DAO.bases.Values;

import java.util.List;

public class ProducteImpSerializable extends SerializableImplementation<Producte, Long>
{
    public ProducteImpSerializable() { super(Values.FILE_PRODUCTE); }

    @Override
    public Producte get(Long valueId)
    {
        List<Producte> productes = getWhole();
        for (Producte producte : productes)
        {
            if(producte.getIdProducte().equals(valueId)) return producte;
        }
        return null;
    }
}
