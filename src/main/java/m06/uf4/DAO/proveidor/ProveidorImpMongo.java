package m06.uf4.DAO.proveidor;

import com.mongodb.client.model.Updates;
import m06.uf4.DAO.bases.MongoImplementation;
import m06.uf4.DAO.bases.Values;

public class ProveidorImpMongo extends MongoImplementation<Proveidor, Long>
{
    public ProveidorImpMongo(String ip) {
        super(ip, Values.DATABASE, Values.COLLECTION_PROVEIDOR, "idProv", Proveidor.class);
    }

    public ProveidorImpMongo(String ip, boolean isServer) { super(ip, isServer, Values.DATABASE, Values.COLLECTION_PROVEIDOR, "idProv", Proveidor.class); }

    @Override
    public boolean modify(Proveidor value)
    {
        return mongo.updateDocumentById(database, collection, value.getIdProv() + "", Updates.combine(
                Updates.set("nom", value.getNom()),
                Updates.set("adreca", value.getAdreca()),
                Updates.set("ciutat", value.getCiutat()),
                Updates.set("estat", value.getEstat()),
                Updates.set("codiPostal", value.getCodiPostal()),
                Updates.set("area", value.getArea()),
                Updates.set("telefon", value.getTelefon()),
                Updates.set("idProducte", value.getIdProducte()),
                Updates.set("quantitat", value.getQuantitat()),
                Updates.set("limitCredit", value.getLimitCredit()),
                Updates.set("observacions", value.getObservacions())
        ));
    }
}
