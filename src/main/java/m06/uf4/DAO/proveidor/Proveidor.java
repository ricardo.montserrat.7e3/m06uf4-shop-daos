package m06.uf4.DAO.proveidor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;

/*
    ID_PROV      int(6) unsigned        not null, primary key,
    NOM          varchar(45)            not null,
    ADRECA       varchar(40)            not null,
    CIUTAT       varchar(30)            not null,
    ESTAT        varchar(2)             null,
    CODI_POSTAL  varchar(9)             not null,
    AREA         smallint(3)            null,
    TELEFON      varchar(9)             null,
    ID_PRODUCTE  int(6) unsigned        null,
    QUANTITAT    int(8) unsigned        null,
    LIMIT_CREDIT decimal(9, 2) unsigned null,
    OBSERVACIONS text                   null
     */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Proveidor implements Serializable
{
    private Long idProv;
    private String nom, adreca, ciutat, estat, codiPostal;
    private Integer area;
    private String telefon;
    private Long idProducte, quantitat;
    private Double limitCredit;
    private String observacions;
}
