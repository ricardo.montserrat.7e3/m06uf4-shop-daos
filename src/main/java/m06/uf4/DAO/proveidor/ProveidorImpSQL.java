package m06.uf4.DAO.proveidor;

import m06.uf4.DAO.bases.SQLImplementation;
import m06.uf4.DAO.bases.Values;

import java.math.BigDecimal;
import java.util.ArrayList;

// EMPLEAT_ID, COGNOM, OFICI, CAP, DATA_ALTA, SALARI, COMISSIO, DEPT_NO

public class ProveidorImpSQL extends SQLImplementation<Proveidor, Long>
{
    public ProveidorImpSQL()
    {
        super(Values.COLLECTION_PROVEIDOR, "id_prov", 12);
    }

    @Override
    public Object[] getObjectParameters(Proveidor value)
    {
        return new Object[]
                {
                        value.getIdProv(),
                        value.getNom(),
                        value.getAdreca(),
                        value.getCiutat(),
                        value.getEstat(),
                        value.getCodiPostal(),
                        value.getArea(),
                        value.getTelefon(),
                        value.getIdProducte(),
                        value.getQuantitat(),
                        value.getLimitCredit(),
                        value.getObservacions()
                };
    }

    @Override
    public Proveidor castToObject(ArrayList<Object> params)
    {
        Proveidor proveidor = new Proveidor();

        Object value = params.get(0);
        proveidor.setIdProv(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(1);
        proveidor.setNom(value != null? (String) value : null);

        value = params.get(2);
        proveidor.setAdreca(value != null? (String) value : null);

        value = params.get(3);
        proveidor.setCiutat(value != null ? (String) value : null);

        value = params.get(4);
        proveidor.setEstat(value != null ? (String) value : null);

        value = params.get(5);
        proveidor.setCodiPostal(value != null ? (String) value : null);

        value = params.get(6);
        proveidor.setArea(value != null ? (Integer) value : null);

        value = params.get(7);
        proveidor.setTelefon(value != null ? (String) value : null);

        value = params.get(8);
        proveidor.setIdProducte(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(9);
        proveidor.setQuantitat(value != null ? value instanceof Integer? (Integer) value : (Long) value : null);

        value = params.get(10);
        proveidor.setLimitCredit(value != null ? value instanceof Double ? (Double) value : ((BigDecimal) value).doubleValue() : null);

        value = params.get(11);
        proveidor.setObservacions(value != null ? (String) value : null);

        return proveidor;
    }

    @Override
    public boolean modify(Proveidor value)
    {
        try
        {
            sqlPetition(String.format(
                    """
                            UPDATE %s 
                            SET id_prov = ?, nom = ?, adreca = ?, ciutat = ?, estat = ?, codi_postal = ?, area = ?, telefon = ?, id_producte = ?, quantitat = ?, limit_credit = ?, observacions = ?
                            WHERE id_prov = ?
                            """,
                    tableName), columns, true, getObjectParameters(value), value.getIdProv());
            return true;
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
            e.printStackTrace();
            return false;
        }
    }
}
