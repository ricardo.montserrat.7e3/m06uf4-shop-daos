package m06.uf4.DAO.proveidor;

import m06.uf4.DAO.bases.SerializableImplementation;
import m06.uf4.DAO.bases.Values;

import java.util.List;

public class ProveidorImpSerializable extends SerializableImplementation<Proveidor, Long>
{
    public ProveidorImpSerializable() { super(Values.FILE_PROVEIDOR); }

    @Override
    public Proveidor get(Long valueId)
    {
        List<Proveidor> proveidors = getWhole();
        for (Proveidor proveidor : proveidors)
        {
            if(proveidor.getIdProv().equals(valueId)) return proveidor;
        }
        return null;
    }
}
