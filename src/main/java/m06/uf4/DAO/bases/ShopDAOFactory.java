package m06.uf4.DAO.bases;

public interface ShopDAOFactory<ComandaImp, EmpleatImp, ProducteImp, ProveidorImp>
{
    ComandaImp getComandaImplementation();

    EmpleatImp getEmpleatImplementation();

    ProducteImp getProducteImplementation();

    ProveidorImp getProveidorImplementation();
}
