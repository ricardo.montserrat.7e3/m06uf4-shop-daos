package m06.uf4.DAO.bases;

import m06.uf4.dependencies.MyMongoDatabase;

import java.beans.PropertyChangeSupport;
import java.util.List;

public abstract class MongoImplementation<T, id> implements DAO<T, id>
{
    public String database, collection;
    protected final MyMongoDatabase mongo;

    protected final Class<T> type;
    protected final String idField;

    protected PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public MongoImplementation(final String ip, final String database, final String collection, final String idField, final Class<T> type)
    {
        mongo = new MyMongoDatabase(ip);
        this.idField = idField;
        this.database = database;
        this.collection = collection;
        this.type = type;
    }

    public MongoImplementation(final String ip, final boolean isServer, final String database, final String collection, final String idField, final Class<T> type)
    {
        mongo = new MyMongoDatabase(ip, isServer);
        this.idField = idField;
        this.database = database;
        this.collection = collection;
        this.type = type;
    }

    @Override
    public boolean insert(T value)
    {
        try
        {
            mongo.insertDocument(database, collection, value);
        }
        catch (Exception e) { System.out.printf("Couldn't insert %s cause %s%n", value, e); return false; }
        return true;
    }

    @Override
    public int insert(List<T> values)
    {
        mongo.insertDocuments(database, collection, values);
        return values.size();
    }

    @Override
    public boolean delete(id valueId) { return mongo.deleteDocumentById(database, collection, valueId + ""); }

    @Override
    public boolean deleteWhole() { return mongo.dropCollection(database, collection) > 0; }

    @Override
    public abstract boolean modify(T value);

    @Override
    public T get(id valueId)
    {
        return mongo.getDocumentById(database, collection, idField, valueId + "", type);
    }

    @Override
    public List<T> getWhole()
    {
        return mongo.getDocuments(database, collection, type);
    }
}
