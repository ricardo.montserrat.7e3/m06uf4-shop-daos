package m06.uf4.DAO.bases;

import java.io.File;

public class Values
{
    //SQL
    public final static String URL = "jdbc:mysql://192.168.56.101:3306/";
    public final static String DB = Values.DATABASE;
    public final static String USER = "user1";
    public final static String PASSWORD = "password1";

    //MONGO
    public static final String IP_MONGO_MACHINE = "192.168.56.101";
    public static final String DATABASE = "shop";

    public static final String COLLECTION_PROVEIDOR = "PROV";
    public static final String COLLECTION_PRODUCTE = "PRODUCTE";
    public static final String COLLECTION_COMANDA = "COMANDA";
    public static final String COLLECTION_EMPLEAT = "EMPLEAT";

    //SERIALIZABLE
    public static final String FILES_FOLDER = "shopFitxers" + File.separator;

    public static final String FILE_PROVEIDOR = FILES_FOLDER + "proveidors.dat";
    public static final String FILE_PRODUCTE = FILES_FOLDER + "productes.dat";
    public static final String FILE_COMANDA = FILES_FOLDER + "comandas.dat";
    public static final String FILE_EMPLEAT = FILES_FOLDER + "empleats.dat";
}
