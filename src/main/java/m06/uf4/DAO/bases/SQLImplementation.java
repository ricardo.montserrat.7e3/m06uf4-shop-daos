package m06.uf4.DAO.bases;

import m06.uf4.factories.ShopDAOFactorySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class SQLImplementation<T, id> implements DAO<T, id>
{
    protected final Connection connection;

    protected final String tableName;
    protected final String idColumnName;

    protected final int columns;

    public SQLImplementation(String tableName, String idColumnName, int columns)
    {
        connection = ShopDAOFactorySQL.createConnection();
        this.idColumnName = idColumnName;
        this.tableName = tableName;
        this.columns = columns;
    }

    public abstract Object[] getObjectParameters(T value);

    public abstract T castToObject(ArrayList<Object> params);

    protected ArrayList<ArrayList<Object>> sqlPetition(String query, int columns, boolean dataManipulation, Object... params) throws SQLException
    {
        ArrayList<ArrayList<Object>> result = new ArrayList<>();
        PreparedStatement stmt = connection.prepareStatement(query);

        System.out.println(query + " params: " + params.length);
        for (int i = 0; i < params.length; i++)
        {
            if (params[i] instanceof Object[] array)
            {
                for (Object o : array)
                {
                    stmt.setObject(i + 1, o);
                    i++;
                }
            }
            else stmt.setObject(i + 1, params[i]);
        }

        if (dataManipulation)
        {
            stmt.executeUpdate();
            return result;
        }

        ResultSet rs = stmt.executeQuery();
        while (rs.next())
        {
            ArrayList<Object> thisRow = new ArrayList<>();

            for (int column = 1; column <= columns; column++) thisRow.add(rs.getObject(column));

            result.add(thisRow);
        }
        return result;
    }

    protected String getQuestionMarks(int quantity, boolean betweenParenthesis)
    {
        StringBuilder stringBuilder = new StringBuilder();
        if (betweenParenthesis) stringBuilder.append('(');
        for (int i = 0; i < quantity; i++)
        {
            stringBuilder.append('?');
            if (i != quantity - 1) stringBuilder.append(',');
        }
        if (betweenParenthesis) stringBuilder.append(')');
        return stringBuilder.toString();
    }

    @Override
    public boolean insert(T value)
    {
        try
        {
            sqlPetition("INSERT INTO " + tableName + " VALUES " + getQuestionMarks(columns, true), columns, true, getObjectParameters(value));
            return true;
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int insert(List<T> values)
    {
        int counter = 0;
        for (T value : values)
        {
            if (insert(value)) counter++;
        }
        return counter;
    }

    @Override
    public boolean delete(id valueId)
    {
        try
        {
            sqlPetition("DELETE FROM " + tableName + " WHERE " + idColumnName + "= ?", columns, true, valueId);
            return true;
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteWhole()
    {
        try
        {
            sqlPetition("DELETE FROM " + tableName, columns, true);
            return true;
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public abstract boolean modify(T value);

    @Override
    public T get(id valueId)
    {
        try
        {
            return castToObject(sqlPetition("SELECT * FROM " + tableName + " WHERE " + idColumnName + "= ?", columns, false, valueId).get(0));
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<T> getWhole()
    {
        try
        {
            List<T> values = new ArrayList<>();
            for (ArrayList<Object> o : sqlPetition("SELECT * FROM " + tableName, columns, false)) values.add(castToObject(o));
            return values;
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
            e.printStackTrace();
            return null;
        }
    }
}
