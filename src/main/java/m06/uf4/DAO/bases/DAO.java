package m06.uf4.DAO.bases;


import java.util.List;

public interface DAO<T, id>
{
    boolean insert(T value);

    int insert(List<T> values);

    boolean delete(id valueId);

    boolean deleteWhole();

    boolean modify(T value);

    T get(id valueId);

    List<T> getWhole();
}
