package m06.uf4.DAO.bases;

import java.beans.PropertyChangeSupport;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class SerializableImplementation<T, id> implements DAO<T, id>
{
    private final File file;

    protected PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public SerializableImplementation(String filePath)
    {
        file = new File(filePath);
        if (!file.exists()) createFile(file);
    }

    private void createFile(File file)
    {
        File parentDirectories = file.getParentFile();

        if (parentDirectories != null) System.out.println(parentDirectories.mkdirs() ? "Successfully created parent directories" : "Couldn't create parent directories");
        try
        {
            System.out.println(file.createNewFile() ? "Created file! " + file.getName() : "File already exist: ");
        }
        catch (IOException e) { e.printStackTrace(); }
    }

    @Override
    public boolean insert(T value)
    {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file))) { objectOutputStream.writeObject(value); }
        catch (Exception e)
        {
            System.out.println("Exception: " + e);
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public int insert(List<T> values)
    {
        int inserted = 0;
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file)))
        {
            for (T value : values)
            {
                objectOutputStream.writeObject(value);
                inserted++;
            }
        }
        catch (Exception e)
        {
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean delete(id valueId)
    {
        return false;
    }

    @Override
    public boolean deleteWhole()
    {
        return false;
    }

    @Override
    public boolean modify(T value)
    {
        return false;
    }

    @Override
    public abstract T get(id valueId);

    @Override
    public List<T> getWhole()
    {
        List<T> result = new ArrayList<>();
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file)))
        {
            T value;
            while ((value = (T) objectInputStream.readObject()) != null) result.add(value);
        }
        catch (Exception e)
        {
            System.out.println("Finished reading file! " + file.getName());
        }
        return result;
    }
}
